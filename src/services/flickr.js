import fetchJsonP from 'fetch-jsonp';

const baseFlickrUrl = 'https://api.flickr.com/services/feeds/photos_public.gne?format=json&tagmode=any';
const flickrParams = {
  jsonpCallbackFunction: 'jsonFlickrFeed',
};

const fetchPhotosByTag = async (tagInputValue) => {
  try {
    const data = await fetchJsonP(`${baseFlickrUrl}&tags=${tagInputValue}`, flickrParams);
    const json = await data.json();
    return json.items;
  } catch (err) {
    console.error(err);
  }
};

export default fetchPhotosByTag;
