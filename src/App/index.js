import React, { Component } from 'react';
import fetchPhotosByTag from '../services/flickr';
import TagInput from '../TagInput';
import Photo from '../Photo';
import { AppWrapper, Photos } from './components';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tagInputValue: 'ANZ',
      photos: [],
    };
    this.handleTagInputChange = this.handleTagInputChange.bind(this);
    this.handleTagInputSubmit = this.handleTagInputSubmit.bind(this);
  }

  async componentDidMount() {
    this.fetchPhotos();
  }

  async fetchPhotos() {
    const photos = await fetchPhotosByTag(this.state.tagInputValue);
    this.setState({
      photos,
    });
  }

  handleTagInputChange(event) {
    this.setState({
      tagInputValue: event.target.value,
    });
  }

  handleTagInputSubmit(event) {
    event.preventDefault();
    this.fetchPhotos();
  }

  render() {
    return (
      <AppWrapper>
        <TagInput
          inputValue={this.state.tagInputValue}
          handleChange={this.handleTagInputChange}
          handleSubmit={this.handleTagInputSubmit}
        />
        <Photos>
          {this.state.photos.map(photo => (
            <Photo
              data={photo}
              key={photo.date_taken}
            />
          ))}
        </Photos>
      </AppWrapper>
    );
  }
}

export default App;
