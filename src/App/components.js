import styled from 'react-emotion';

export const AppWrapper = styled('div')`
padding: 10px;
`;

export const Photos = styled('div')`
display: flex;
width: 100%;
flex-wrap: wrap;
justify-content: center;
`;
