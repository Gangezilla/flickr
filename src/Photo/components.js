import styled from 'react-emotion';

export const PhotoWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  padding: 15px;
  border-radius: 5px;
  border: 2px solid #ddd;
  margin: 25px 10px 0 0;
  background-color: #fff;

  @media (min-width: 768px) {
    width: 330px;
  }
  @media (min-width: 1024px) {
    width: 290px;
  }
`;

export const Title = styled('h1')`
  font-size: 16px;
`;

export const Author = styled('p')`
  font-size: 14px;
  margin: 0 0 10px 0;
`;

export const ImageLink = styled('a')`
  align-self: center;
`;

export const Tags = styled('div')`
  display: flex;
  flex-wrap: wrap;
`;

export const Tag = styled('a')`
  margin-right: 10px;
  text-decoration: none;
  background-color: #ff6257;
  color: #fff;
  padding: 2px 5px;
  margin: 0 3px 3px 0;

  &:hover {
    background-color: #d63c0a;
  }
`;

export const PhotoDate = styled('p')`
  font-size: 14px;
  margin: 0 0 10px 0;
`;
