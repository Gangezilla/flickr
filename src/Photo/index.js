import React from 'react';
import { PhotoWrapper, Title, Author, ImageLink, Tags, Tag, PhotoDate } from './components';

const createTagElements = (tags) => {
  const first10Tags = tags.slice(0, 10);
  return first10Tags.map((tag) => {
    const link = `https://www.flickr.com/photos/tags/${tag}`;
    return (
      <Tag
        href={link}
        key={tag}
      >
        <span>{tag}</span>
      </Tag>
    );
  });
};

const Photo = ({ data }) => {
  const date = new Date(data.date_taken).toDateString();
  const tags = data.tags.split(' ');

  return (
    <PhotoWrapper>
      <ImageLink
        href={data.link}
      >
        <img alt={data.title} src={data.media.m} />
      </ImageLink>
      <Title>{data.title}</Title>
      <Author>By: {data.author}</Author>
      <PhotoDate>Posted on: {date}</PhotoDate>
      <Tags>
        {createTagElements(tags)}
      </Tags>
    </PhotoWrapper>
  );
};

export default Photo;
