import React from 'react';
import { FormWrapper, Input, Submit, Fixed } from './components';

const TagInput = ({ inputValue, handleChange, handleSubmit }) => {
  return (
    <Fixed>
      <FormWrapper onSubmit={handleSubmit}>
        <Input
          value={inputValue}
          onChange={handleChange}
          type="text"
          placeholder="Tag"
        />
        <Submit type="submit" value="Submit" />
      </FormWrapper>
    </Fixed>
  );
};

export default TagInput;
