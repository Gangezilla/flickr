import styled from 'react-emotion';

export const FormWrapper = styled('form')`
  display: flex;
  justify-content: center;
  flex-direction: column;
  max-width: 450px;
  align-items: center;
  margin: 0 auto;
`;

export const Input = styled('input')`
  font-size: 18px;
  width: 100%;
  padding: 10px;
`;

export const Submit = styled('input')`
  width: 250px;
  align-self: center;
  background: #52BD9C;
  color: white;
  font-size: 16px;
  margin-top: 15px;
  height: 40px;
  border-radius: 5px;
`;

export const Fixed = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100vw;
  line-height: 60px;
  padding: 10px;
  background-color: #ffffff;
  z-index: 2;
  box-shadow: 0px 12px 12px -12px #141414;
`;
