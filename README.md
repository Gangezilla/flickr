# flickr-app

Hi! This is the little application I put together for you guys, you can view it here: pink-representative.surge.sh
Or you can run `yarn build` to build it and do whatever you need to with it.

It's been put together using create-react-app and vanilla React. I didn't end up using Redux because I didn't really get enough time to integrate it properly, plus I didn't feel like this application needed Redux. To me, Redux is necessary if you've built a very complex application, such as a very complicated form. The state management for this application is not very complex, so I wanted to keep it simple.

If I had a bit more time:

- I'd work on making it prettier, my design skills aren't too great these days, but it looks okay.
- Limited breakpoints right now
- Adding pagination, currently only showing 20 images per request.
- Add error checking for flickr, instead of just logging out an error.
- Add some tests, everything is chunked up into components so it wouldn't be too hard to add some tests using Enzyme to test the React components, as well as some basic tests for the Flickr client as well.
- Make it clear in the UI that you can search for multiple things with a comma seperated string.
- Make the flickr fetch safer, proper error checking.
- Make URL a URL object instead of just string appending.

Overall, took about 3/4 hours, but like 1 of those hours was spent fiddling with the Flickr API, JSONP and the JSONP callback stuff which took a bit longer than I would have liked 😅